# PropGenix

Next generation property management software that aims to empower and educate landlords and property managers.

## Developer Dependencies

1. Node.js version 12.4.0
2. Mongo Database 3.4

## Installation Steps

1. Install yarn
        npm install -g yarn
2. Install all of the node modules
        yarn install
3. Run the run.sh script
        ./run.sh
