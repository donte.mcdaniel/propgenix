'use strict'

const express = require('express');
const router = express.Router();
const passport = require('passport');
const authHandler = require('./src/server/Handlers/Authentication/AuthHandler');
const tokenList = {};

router.use(express.static(__dirname + '/public'));

/************************** All Gets ***********************************/
// Home Page
router.get('/', function(req, res) {
  res.send({
    name: 'PropGenix Node App',
    status: 'Running'
  });
});

// Post handler for logging in user 
router.post('/login', async(req, res, next) => {
  passport.authenticate('login', async(err, user, info) => {
    try {
      if (err || !user) {
        const error = new Error('Login credentials are incorrect or user does not exist');
        return next(error);
      }
      req.login(user, { session:false }, async(error) => {
        if (error) return next(error);

        let response = authHandler.createJWT(user, 'Login Successful');
        tokenList[response.refreshToken] = response;
        return res.json(response);
      });
    }
    catch (error) {
      return next(error);
    }
  })(req, res, next);
});

// Post handler for signing up user 
router.post('/signup', passport.authenticate('signup', { session: false }), async(req, res, next) => {
  let response = authHandler.createJWT(req.user, 'Signup Successful');
  tokenList[response.refreshToken] = response;
  return res.json(response);
});

module.exports = router;
