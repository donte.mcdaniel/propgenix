'use strict';

const http = require('http');
const express = require('express');
const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const authHandler = require('./src/server/Handlers/Authentication/AuthHandler');
const connectionMap = new Map;
const DBConnection = mongoose.connection;
const DBURL = process.env.DATABASE_URL;
const secret = process.env.AUTH_SECRET;
const logger = require('./src/server/Utilities/logger');
require('./src/server/Authentication/Middleware/auth');

// Handlers
const leadsHandler = require('./src/server/Handlers/Leads/LeadsHandler');
const emailHandler = require('./src/server/Handlers/EmailHandler');
const AuthHandler = require('./src/server/Handlers/Authentication/AuthHandler');
const propertiesHandler = require('./src/server/Handlers/Properties/Property-Handler');

let routes = require('./routes');

connectMongo();

app.set('port', process.env.PORT || 5000);
app.use(express.static(__dirname + '/public'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(session({
  secret: secret,
  resave:true,
  saveUninitialized:false
}));

app.use('/', routes);

// Starting the web server
server.listen(app.get('port'), function() {
  logger.info("PropGenix is running on " + app.get('port'));
});

// Socket connection handler
io.on('connection', function(socket) {
  addConnection(socket);
  initializeHandlers(socket);
  socket.on('disconnect', () => {
      logger.info('Socket Disconnected. Removing Connection');
      removeConnection(socket);
  });
  socket.on('error', connectionError);
  socket.on('addTokensToConnection', tokens => {
    socket.token = tokens.token;
    socket.refreshToken = tokens.refreshToken;
  });

  socket.on('GetUserData', () => {
    if (AuthHandler.validateJWT(socket.refreshToken)) {
      socket.emit('UserData', AuthHandler.decodeToken(socket.refreshToken));
    }
    else {
      // TODO: Log user out and close websocket
    }
  });
});

function initializeHandlers(socket) {
  new leadsHandler(socket, DBConnection);
  new emailHandler(socket, DBConnection, authHandler);
  new propertiesHandler(socket, DBConnection);
}

/**
 * END ALL SOCKET CONNECTIONS
 */

function connectionError() {
    logger.error('Error in client connection');
    logger.error(err.stack || err);
}

function connectMongo() {
  mongoose.connect(DBURL, {
    useNewUrlParser: true,
    useCreateIndex: true
  });
  mongoose.set('useFindAndModify', false);
  mongoose.Promise = global.Promise;
  DBConnection.once('open', function() {
    logger.info('Database Connected!');
  });
  DBConnection.once('error', function() {
    logger.error('Database Error!');
  })
  DBConnection.once('reconnected', function() {
    logger.info('Database Reconnected!');
  })
  DBConnection.once('disconnected', function() {
    logger.info('Database disconnected!');
  })
}

/**
 * 
 * @param {*} connection Socket IO client connection
 */
function addConnection(connection) {
  connectionMap.set(connection.id, connection);
}

/**
 * 
 * @param {*} connection Socket IO client connection
 */
function removeConnection(connection) {
  connectionMap.delete(connection.id);
}

process.on('uncaughtException', (err) => {
  logger.error('There was an uncaught error', err)
  process.exit(1) //mandatory (as per the Node docs)
});
