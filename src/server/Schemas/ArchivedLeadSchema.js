'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const archivedLeadsSchema = Schema({
  lead: {
    type: Object,
    required: true
  },
  expireAt: {
    type: Date,
    default: Date.now,
    index: { expires: '43800m' }, // 1 month
  },
});

module.exports = mongoose.model('archivedLeads', archivedLeadsSchema);
