'use strict'

const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const propertiesSchema = Schema({
  _id: mongoose.Schema.Types.ObjectId,
  userId: {
    type: String,
    required: true
  },
  propertyType: { // Multi-Unit, Apartment, Condo, Townhouse, Single Family Home, Commercial
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true
  },
  state: {
    type: String,
    required: true
  },
  zipcode: {
    type: String,
    required: true
  },
  bedrooms: {
    type: Number,
    required: true
  },
  bathrooms: {
    type: Number,
    required: true
  },
  squarefootage: {
    type: Number,
    required: true
  },
  basement: Boolean,
  status: String, // Occupied, Vacant, Available
  date_added: String
});

propertiesSchema.pre('save', function(next) {
  let currentDate = moment.unix(moment().unix()).format('MM/DD/YYYY, h:mm:ss a');
  
  this._id = mongoose.Types.ObjectId();
  if (!this.date_created) {
    this.date_added = currentDate;
  }
  next();
});

module.exports = mongoose.model('Properties', propertiesSchema);
