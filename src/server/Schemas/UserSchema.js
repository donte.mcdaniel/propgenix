'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto');

let userSchema = Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  salt: String,
  password: {
    type: String,
    required: true
  },
  role: String,
  created_at: Date,
  admin: false
});

userSchema.pre('save', function(next) {
  let currentDate = new Date();
  this._id = mongoose.Types.ObjectId();
  this.setPassword(this.password);
  if (!this.created_at) {
    this.created_at = currentDate;
  }
  next();
});

userSchema.methods.setPassword = function(pass) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.password = crypto.pbkdf2Sync(pass, this.salt, 1000, 64, 'sha512').toString('hex');
}

userSchema.methods.validPassword = function(pass) {
  var hash = crypto.pbkdf2Sync(pass, this.salt, 1000, 64, 'sha512').toString('hex');
  return this.password === hash;
}

module.exports = mongoose.model('User', userSchema);
