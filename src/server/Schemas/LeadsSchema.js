'use strict'

const mongoose = require('mongoose');
const moment = require('moment');
const Schema = mongoose.Schema;

const leadsSchema = Schema({
  _id: mongoose.Schema.Types.ObjectId,
  userId: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  },
  phone: {
    type: String,
    required: true
  },
  property: {
    type: String,
    required: true
  },
  status: String,
  lastContact: String,
  lastAttempt: String,
  unitPreference: {
    bedrooms: Number,
    bathrooms: Number,
    leaseLength: Number,
    rentRangeMin: Number,
    rentRangeMax: Number
  },
  additionalInformation: {
    contactMethod: String,
    gender: String,
    referrer: String,
    leaseType: String, // Standard, Employee, Military, Courtesy Officer
    moveInDate: String
  },
  date_created: String
});

leadsSchema.pre('save', function(next) {
  let currentDate = moment.unix(moment().unix()).format('MM/DD/YYYY, h:mm:ss a');
  
  this._id = mongoose.Types.ObjectId();
  if (!this.date_created) {
    this.date_created = currentDate;
    this.lastContact = currentDate;
    this.lastAttempt = currentDate;
    this.status = 'Contact Created';
  }
  next();
});

module.exports = mongoose.model('Leads', leadsSchema);
