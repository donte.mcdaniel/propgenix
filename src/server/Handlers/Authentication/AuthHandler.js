'use strict'

const useAuth = process.env.USE_AUTH;
const secret = process.env.AUTH_SECRET;
const tokenLife = process.env.TOKEN_LIFE;
const refreshTokenLife = process.env.REFRESH_TOKEN_LIFE;
const jwt = require('jsonwebtoken');

/**
 * 
 * @param {Object} user
 * @param {Object} message
 * @returns {Object}
 */
function createJWT(user, message) {
  const body = { _id: user._id, email: user.email, name: user.name };
  const token = jwt.sign({user: body}, secret, { expiresIn: tokenLife });
  const refreshToken = jwt.sign({user: body}, secret, { expiresIn: refreshTokenLife });
  const response = {
    'status': message,
    'token': token,
    'refreshToken': refreshToken
  };
  return response;
}

/**
 * 
 * @param {String} token 
 * @returns {Boolean}
 */
function validateJWT(token) {
  if (useAuth === 'false') {
    return true;
  }
  else {
    var decodedToken = decodeToken(token);
    if (decodedToken.exp < new Date().getTime()/1000) {
      return false;
    }
    else {
      return true;
    }
  }
}

/**
 * 
 * @param {String} token 
 */
function decodeToken(token) {
  var parts = token.split('.');
  var partsToConvert = parts[1] + '===='.substring(parts[1].length % 4);
  var decodedPart = new Buffer(partsToConvert, 'base64').toString('UTF8');
  var obj = JSON.parse(decodedPart);
  return obj;
}

/**
 * Gets the userId from the token
 * @param {String} token
 * @returns {String}
 */
function getUserId(token) {
  let decodedToken = decodeToken(token);
  return decodedToken.user._id;
}

module.exports = {
  decodeToken,
  validateJWT,
  getUserId,
  createJWT
};
