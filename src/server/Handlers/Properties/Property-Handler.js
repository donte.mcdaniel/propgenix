'use strict'

const Properties = require('../../Schemas/PropertiesSchema');
const logger = require('../../Utilities/logger');
const AuthHandler = require('../Authentication/AuthHandler');
const moment = require('moment');
const mock_data = require('./property_mock_data.json');

class PropertiesHandler {
  constructor(socket, databaseConnection) {
    // TODO: Make socket private by adding _
    this._socket = socket;
    this._databaseConnection = databaseConnection;
    this.socketHandler();
    // console.log(mock_data);
    // mock_data.forEach(property => {
    //   this.AddProperty(property);
    // });
    
  }

  /**
   * Listener for all sockets events specific for properties
   */
  socketHandler() {
    // Get All Properties
    this._socket.on('getAllProperties', () => {
      if (AuthHandler.validateJWT(this._socket.refreshToken)) { // Check to see if token is valid
        const userId = AuthHandler.getUserId(this._socket.refreshToken);
        this.GetAllProperties(userId).then(data => {
          if (data.code === 11000 || data.code === 11001) {
            this._socket.emit('GetPropertiesError', data);
          }
          else {
            this._socket.emit('AllProperties', data);
          }
        });
      }
      else {
        logger.error('Token has expired. Logging user out.');
        // TODO: Log user out
      }
    });

    // Add Property
    this._socket.on('addProperty', (data) => {
      console.log('Adding Property');
      console.log(data);
      // if (AuthHandler.validateJWT(this._socket.refreshToken)) { // Check to see if token is valid
      //   const userId = AuthHandler.getUserId(this._socket.refreshToken);
      //   this.AddProperty(userId).then(data => {
      //     if (data.code === 11000 || data.code === 11001) {
      //       this._socket.emit('GetPropertiesError', data);
      //     }
      //     else {
      //       this._socket.emit('AllProperties', data);
      //     }
      //   });
      // }
      // else {
      //   logger.error('Token has expired. Logging user out.');
      //   // TODO: Log user out
      // }
    });
  }

  /**
   * @param {String} userId
   * @returns {Promise}
   */
  GetAllProperties(userId) {
    let Properties = this._databaseConnection.models.Properties;
    return new Promise(function(resolve, reject) {
      Properties.find({'userId': userId}, function(err, properties) {
        if (err) {
          reject(err);
        }
        else {
          resolve(properties);
        }
      });
    });
  }

  /**
   * @param {Object} property
   * @returns {Promise}
   */
  AddProperty(property) {
    return new Promise(function(resolve, reject) {
      let newProperty = new Properties;
      newProperty.userId = property.userId;
      for (var key in property) {
        // check if the property/key is defined in the object itself, not in parent
        if (property.hasOwnProperty(key)) {
          if (property[key]) {
            newProperty[key] = property[key];
          }
        }
      }
      newProperty.save(function (err) {
        if(err) {
          reject(err);
        }
        else {
          resolve(newProperty);
        }
      });
    });
  }
}

module.exports = PropertiesHandler;
