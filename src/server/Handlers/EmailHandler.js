'use strict'

const fs = require('fs');
const fetch = require("node-fetch");

class EmailHandler {
  constructor(socket) {
    this.socket = socket;

    this.registerSockets();
  }

  registerSockets() {
    this.socket.on('sendTestEmail', () => {
      console.log('Test Email Sent');
      this.sendTestEmail();
    });
  }

  sendTestEmail() {
    // Send Post to api service
    let data = {};
    let url = 'http://localhost:3000/token/sample_token/function/sendTestEmail';
    let request = makeRequest(url, 'POST', data);
    request.then(data => {
      if (data.code === 11000 || data.code === 11001) {
        this.socket.emit('EmailError', data);
      }
      else {
        this.socket.emit('EmailSent', data);
      }
    });
  }

  // getAllLeads() {
  //   let url = 'http://localhost:3000/token/sample_token/function/getAllLeads';
  //   let request = makeRequest(url, 'GET');
  //   request.then(data => {
  //     if (data.code === 11000 || data.code === 11001) {
  //       this.socket.emit('GetLeadError', data);
  //     }
  //     else {
  //       this.socket.emit('AllLeads', data);
  //     }
  //   });
  // }
}

/**
* @returns {Promise}
* @param {String} url
* @param {String} type POST or GET
* @param {Object} options
*/
function makeRequest(url, type, data) {
  if (type === 'GET') {
      return new Promise(function(resolve, reject) {
          fetch(url).then(response => {
            response.json().then(d => {
              resolve(d);
            });
          }).catch(error => {
              reject(error);
          });
      });
  }
  else {
      return new Promise(function(resolve, reject) {
          fetch(url, {
            method: 'post',
            body:    JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
          }).then(response => {
              response.json().then(json => {
                  resolve(json);
              });
          }).catch(error => {
              reject(error);
          });
      });
  }
};

module.exports = EmailHandler;
