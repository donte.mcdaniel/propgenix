'use strict'

const Leads = require('../../Schemas/LeadsSchema');
const ArchivedLead = require('../../Schemas/ArchivedLeadSchema');
const logger = require('../../Utilities/logger');
const AuthHandler = require('../Authentication/AuthHandler');
const moment = require('moment');

class LeadsHandler {
  constructor(socket, databaseConnection) {
    // TODO: Make socket private by adding _
    this._socket = socket;
    this._databaseConnection = databaseConnection;
    this.socketHandler();
  }

  /**
   * Listener for all sockets events specific for leads
   */
  socketHandler() {
    // Get All Leads
    this._socket.on('getAllLeads', () => {
      if (AuthHandler.validateJWT(this._socket.refreshToken)) { // Check to see if token is valid
        const userId = AuthHandler.getUserId(this._socket.refreshToken);
        this.GetAllLeads(userId).then(data => {
          if (data.code === 11000 || data.code === 11001) {
            this._socket.emit('GetLeadError', data);
          }
          else {
            this._socket.emit('AllLeads', data);
          }
        });
      }
      else {
        // TODO: Add Error Handler
        logger.error('Token has expired. Logging user out.');
        // TODO: Log user out
      }
    });

    // Add Lead
    this._socket.on('AddLead', (data) => {
      if (AuthHandler.validateJWT(this._socket.refreshToken)) {
        
        this.AddLead(data).then(result => {
          if (result.code === 11000 || result.code === 11001) {
            this._socket.emit('AddLeadError', result);
          }
          else {
            this._socket.emit('AddLeadComplete', result);
          }
        });
      }
    });

    // Update Lead
    this._socket.on('UpdateLead', (data) => {
      if (AuthHandler.validateJWT(this._socket.refreshToken)) {
        this.UpdateLead(data).then(result => {
          if (result.code === 11000 || result.code === 11001) {
            this._socket.emit('UpdateLeadError', result);
          }
          else {
            this._socket.emit('UpdateLeadComplete', result);
          }
        });
      }
    });

    // Archive Lead
    this._socket.on('ArchiveLead', (data) => {
      if (AuthHandler.validateJWT(this._socket.refreshToken)) {
        this.ArchiveLead(data.leadId).then(result => {
          if (result.code === 11000 || result.code === 11001) {
            this._socket.emit('ArchiveLeadError', result);
          }
          else {
            this._socket.emit('ArchiveLeadComplete', result);
          }
        });
      }
    });
  }

  /**
   * @param {Object} lead lead information
   * @returns {Promise}
   */
  AddLead(lead) {
    const userId = AuthHandler.getUserId(this._socket.refreshToken);
    return new Promise(function(resolve, reject) {
      let newLead = new Leads;
      newLead.userId = userId;

      for (var key in lead) {
        // check if the property/key is defined in the object itself, not in parent
        if (lead.hasOwnProperty(key)) {
          if (lead[key]) {
            if (key === 'bedrooms' || key === 'bathrooms' || key === 'leaseLength'|| key === 'rentRangeMin' || 
            key === 'rentRangeMax' || key === 'floorPlan') {
              newLead.unitPreference[key] = lead[key];
            }
            else if (key === 'contactMethod' || key === 'gender' || key === 'referrer'|| key === 'leaseType' || 
            key === 'moveInDate') {
              newLead.additionalInformation[key] = lead[key];
            }
            else {
              newLead[key] = lead[key];
              newLead.unitPreference.bedrooms = null;
              newLead.unitPreference.bathrooms = null;
              newLead.unitPreference.leaseLength = null;
              newLead.unitPreference.rentRangeMin = null;
              newLead.unitPreference.rentRangeMax = null;
              newLead.additionalInformation.contactMethod = null;
              newLead.additionalInformation.gender = null;
              newLead.additionalInformation.referrer = null;
              newLead.additionalInformation.leaseType = null;
              newLead.additionalInformation.moveInDate = null;
            }
          }
        }
      }
      newLead.save(function (err) {
        if(err) {
          reject(err);
        }
        else {
          resolve(newLead);
        }
      });
    });
  }

  UpdateLead(data) {
    let Leads = this._databaseConnection.models.Leads;
    return new Promise(function(resolve, reject) {
      Leads.findOneAndUpdate({_id:data.id}, data, {new:true}, function(err, lead) {
        if (err) {
          reject(err);
        }
        else {
          resolve(lead);
        }
      });
    });
  }

  /**
   * @param {String} userId
   * @returns {Promise}
   */
  GetAllLeads(userId) {
    let Leads = this._databaseConnection.models.Leads;

    return new Promise(function(resolve, reject) {
      Leads.find({'userId': userId}, function(err, leads) {
        if (err) {
          reject(err);
        }
        else {
          resolve(leads);
        }
      });
    });
  }

  /**
   * 
   * @param {String} leadId 
   * @returns {Promise}
   */
  ArchiveLead(leadId) {
    let Leads = this._databaseConnection.models.Leads;

    return new Promise(function(resolve, reject) {
      Leads.findOne({_id: leadId}, function(err, lead) {
        if (err) {
          reject(err);
        }
        else {
          let newArchivedLead = new ArchivedLead;
          newArchivedLead.lead = lead;
          newArchivedLead.save(function (err) {
            if(err) {
              logger.error(err);
            }
            else {
              logger.info('Lead Archived!');
            }
          });
          lead.remove();
          resolve(lead);
        }
      });
    });
  }
}

module.exports = LeadsHandler;
