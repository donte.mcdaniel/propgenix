'use strict'

const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJWT;
const UserModel = require('../../Schemas/UserSchema');
const secret = process.env.AUTH_SECRET;

passport.use('signup', new localStrategy ({
  usernameField: 'email',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, email, password, done) => {
  try {
    const user = await UserModel.create({email, password, name: req.body.name});
    return done(null, user);
  }
  catch (error) {
    done(error);
  }
}));

// TODO: Finish login middleware

passport.use('login', new localStrategy ({
  usernameField: 'email',
  passwordField: 'password'
}, async (email, password, done) => {
  try {
    const user = await UserModel.findOne({email});
    if (!user) {
      return done(null, false, {message: 'User Not Found'});
    }
    const validate = await user.validPassword(password);
    if (!validate) {
      return done(null, false, {message: 'Wrong Password'});
    }
    return done(null, user, {message: 'Login Successful'});
  }
  catch (error) {
    done(error);
  }
}));
