let displays = new Map();
let AppNavs;
let currentDisplay;

/**
 * Init the transitioner.
 */
function init() {
    AppNavs = document.querySelectorAll('li[data-app]:not([value=""])');
    AppNavs.forEach((nodeElement) => {
        registerEvent(nodeElement.dataset.app);
    });
}

function registerEvent(nodeId) {
    let id = document.getElementById('sidebar_'+nodeId);
    id.addEventListener('click', e => {
        if(!displays.has(nodeId)) {
            return;
        }
        let delay = 50;
        transition(nodeId, delay);
    }, false);
}

function transition(name, delay, duration) {
    let display = displays.get(name);
    let displayHeader = document.getElementById('displayHeader');
    displayHeader.innerHTML = toTitleCase(display.displayname);
    if(!display) {
        throw new TypeError(`unknown app: ${name}`);
    }

    let next = display;
    let prev = currentDisplay;

    let p = Promise.resolve();

    if(next === prev) {
        return p;
    }

    if(delay) {
        p = p.then(() => new Promise(r => setTimeout(r, delay)));
    }

    if(prev) {
        p = p.then(() => prev.close());
    }

    return p.then(() => {
        next.element.style.display = '';
        next.element.style['z-index'] = 2;

        if(prev) {
            prev.element.style.display = 'none';
            prev.element.style['z-index'] = 1;
        }

        let n = document.querySelector('li.active');
        if(n) {
            n.classList.remove('active');
        }

        n = document.querySelector(`li[id="sidebar_${name}"]`);
        if(n) {
            n.classList.add('active');
        }

        currentDisplay = next;

        let main = document.getElementById('mainContent');
        main.style.perspective = '1000px';
        return next.transition(duration);
    })
    .then(() => {
        //remove 3d perspective when done
        let main = document.getElementById('mainContent');
        main.style.removeProperty('perspective');
        return next.open();
    }).catch(function(err) {
        console.log(err);
    });
}


/**
 * 
 * @param {display} display The display to add
 * @returns {display} The display
 * displays can be displays
 */
function add(display) {
    if (displays.has(display.displayname)) {
        throw new TypeError(`a display with key ${display.displayname} already exists.`);
    }
    displays.set(display.displayname, display);
}

function remove(display) {
    if (display.delete(display.displaynamee)) {
        return true;
    }
    return false;
}

function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

export default {
    init,
    add,
    remove,
    displays,
    transition
};
