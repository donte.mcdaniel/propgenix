import Display from '../display';

class Maintenance extends Display {
    constructor(ws) {
        super('maintenance');
        this.ws = ws;
        this.displayname = 'maintenance';
    }

    open() {
        this.isopen = true;
        console.log('Maintenance IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default Maintenance;