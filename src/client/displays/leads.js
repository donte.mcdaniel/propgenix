import Display from '../display';
import Table from '../table';
import moment from 'moment';
import eventEmitter from '../event-emitter';

class Leads extends Display {
    constructor(ws) {
      super('leads');
      this.ws = ws;
      this.eventEmitter = eventEmitter;
      this.displayname = 'leads';
      this.leads = new Map();
      this.leadsTable = new Table('leads-table');
      this.registerConnectionCallbacks();
      this.init();
      this.registerListeners();
    }

    registerConnectionCallbacks() {
      this.ws.on('AllLeads', data => {
        data.forEach(d => {
          let convertedDateCreated = moment.unix(d.date_created).format("MM/DD/YYYY");
          let convertedLastContact = moment.unix(d.lastContact).format("MM/DD/YYYY");
          let convertedLastAttempt = moment.unix(d.lastAttempt).format("MM/DD/YYYY");
          d.lastAttempt = convertedLastAttempt;
          d.lastContact = convertedLastContact;
          d.date_created = convertedDateCreated;
        });
        this.leadsTable.addUpdateData(data);
      });
      this.ws.on('AddLeadComplete', data => {
        let convertedTimeStamp = moment.unix(data.date_created).format("MM/DD/YYYY");
        let convertedLastContact = moment.unix(data.lastContact).format("MM/DD/YYYY");
        let convertedLastAttempt = moment.unix(data.lastAttempt).format("MM/DD/YYYY");
        data.date_created = convertedTimeStamp;
        data.lastAttempt = convertedLastAttempt;
        data.lastContact = convertedLastContact;
        this.leadsTable.addUpdateData([data]);

        this.closeClearModal();
      });
      this.ws.on('UpdateLeadComplete', data => {
        let convertedTimeStamp = moment.unix(data.date_created).format("MM/DD/YYYY");
        let convertedLastContact = moment.unix(data.lastContact).format("MM/DD/YYYY");
        let convertedLastAttempt = moment.unix(data.lastAttempt).format("MM/DD/YYYY");
        data.date_created = convertedTimeStamp;
        data.lastAttempt = convertedLastAttempt;
        data.lastContact = convertedLastContact;
        this.leadsTable.updateData(this.currentLead, data);

        this.closeClearModal();
      });
      this.ws.on('AddLeadError', (data) => {
        this.showError(document.getElementById('modal-title-add-edit-lead'), 'Email Exists. Provide Different Email');
      });
    }

    registerListeners() {
      let addEditLead = document.getElementById('addEditLead');
      let leadNewBtn = document.getElementById('leadNewBtn');
      let headerCloseAddEditLeadModal = document.getElementById('headerCloseAddEditLeadModal');
      let addEditLeadCloseBtn = document.getElementById('addEditLeadCloseBtn');
      let downloadpdf = document.getElementById('download-lead-pdf');
      let downloadcsv = document.getElementById('download-lead-csv');
      let downloadjson = document.getElementById('download-lead-json');
      let emailBtn = document.getElementById('emailLeadBtn');

      leadNewBtn.addEventListener('click', () => {
        this.saveMode = 'add';
        this.closeClearModal();
        document.getElementById('modal-title-add-edit-lead').innerHTML = 'Add New Lead';
      });
      addEditLead.addEventListener('click', () => {
        this.saveLead();
      });
      headerCloseAddEditLeadModal.addEventListener('click', () => {
        this.closeClearModal();
      });
      addEditLeadCloseBtn.addEventListener('click', () => {
        this.closeClearModal();
      });
      downloadpdf.addEventListener('click', () => {
        this.leadsTable.downloadTable('pdf', 'Leads');
      });
      downloadcsv.addEventListener('click', () => {
        this.leadsTable.downloadTable('csv');
      });
      downloadjson.addEventListener('click', () => {
        this.leadsTable.downloadTable('json');
      });
      emailBtn.addEventListener('click', () => {
        this.ws.emit('sendTestEmail');
      });

      // Event Emitter Listeners
      this.eventEmitter.on('rowData', (rowData) => {
        // console.log(rowData);
        document.getElementById('modal-title-add-edit-lead').innerHTML = 'Edit Lead';
        this.editLead(rowData);
      });
    }

    /**
     * Gets all neccesary information from database
     */
    init() {
      this.leadsTable.addSelectColumn();
      // let _this = this;
      // this.leadsTable.addColumn('', 'edit', true, false, '', function(cell, formatterParams){
      //   // _this.editLead(cell, formatterParams);
      //   console.log(cell);
      // }, '', 'center');
      this.leadsTable.addColumn('Name', 'name', true, true, 'string');
      this.leadsTable.addColumn('Email', 'email', true, false);
      this.leadsTable.addColumn('Phone', 'phone', true, false);
      this.leadsTable.addColumn('Status', 'status', true, false);
      this.leadsTable.addColumn('Property', 'property', true, false);
      this.leadsTable.addColumn('Date Created', 'date_created', true, true, 'string');
      this.leadsTable.addColumn('Last Contact', 'lastContact', true, true, 'string');

      // Setup Filter
      this.leadsTable.initializeFilter('Name', 'filter-by-field-leads', 'filter-value-leads', 'filter-clear-leads');
      // Setup Event Emitter
      this.leadsTable.setEventEmitter(this.eventEmitter);

      this.ws.emit('getAllLeads');
    }

    /**
     * Saves new leads
     */
    saveLead() {
      let name = document.getElementById('add-edit-lead-name').value;
      let email = document.getElementById('add-edit-lead-email').value;
      let phone = document.getElementById('add-edit-lead-phone').value;
      let agent = document.getElementById('add-edit-lead-agent').value;
      let property = document.getElementById('add-edit-lead-property').value;
      let bedrooms = document.getElementById('add-edit-lead-bedrooms').value;
      let bathrooms = document.getElementById('add-edit-lead-bathrooms').value;
      let leaseLength = document.getElementById('add-edit-lead-lease-length').value;
      let rentRangeMin = document.getElementById('add-edit-lead-rent-range-min').value;
      let rentRangeMax = document.getElementById('add-edit-lead-rent-range-max').value;
      let model = document.getElementById('add-edit-lead-model').value;
      let gender = document.getElementById('add-edit-lead-gender').value;
      let referrer = document.getElementById('add-edit-lead-referrer').value;
      let leaseType = document.getElementById('add-edit-lead-lease-type').value;
      let moveInDate = document.getElementById('add-edit-lead-movein-date').value;
      let contactMethod = document.getElementById('add-edit-lead-contact-method').value;
      
      if (!name) {
        this.showError(document.getElementById('modal-title-add-edit-lead'), 'Please Provide Name');
        throw Error('Please Provide Name');
      }
      if (!email) {
        this.showError(document.getElementById('modal-title-add-edit-lead'), 'Please Provide Email');
        throw Error('Please Provide Email');
      }
      if (!phone) {
        this.showError(document.getElementById('modal-title-add-edit-lead'), 'Please Provide Phone');
        throw Error('Please Provide Phone');
      }
      if (!agent) {
        this.showError(document.getElementById('modal-title-add-edit-lead'), 'Please Provide Agent');
        throw Error('Please Provide Agent');        
      }
      if (!property) {
        this.showError(document.getElementById('modal-title-add-edit-lead'), 'Please Provide Property');
        throw Error('Please Provide Property');
      }
      
      // clear innerHtml
      // remove red color
      document.getElementById('modal-title-add-edit-lead').innerHTML = '';
      document.getElementById('modal-title-add-edit-lead').style.color = 'black';

      let obj = {
        name: name,
        email: email,
        phone: phone,
        agent: agent,
        property: property,
        status: 'Contact Created',
        bedrooms: bedrooms,
        bathrooms: bathrooms,
        leaseLength: leaseLength,
        rentRangeMin: rentRangeMin,
        rentRangeMax: rentRangeMax,
        floorPlan: model,
        gender: gender,
        referrer: referrer,
        leaseType: leaseType,
        moveInDate: moveInDate,
        contactMethod: contactMethod
      }

      if (this.saveMode === 'add') {
        this.ws.emit('AddLead', obj);
      }
      else {
        // Get ID of lead
        obj.id = this.currentLeadId;
        this.ws.emit('UpdateLead', obj);
        // TODO: update the table object
      }
    }
    
    initEdit(cell, formatterParams) {
      console.log(cell);
    }

    editLead(data) {
      this.saveMode = 'edit';
      this.currentLead = data;
      data = data._row;
      this.currentLeadId = data.data._id;
      $('#add-edit-lead-dialog').modal('toggle');
      document.getElementById('add-edit-lead-name').value = data.data.name;
      document.getElementById('add-edit-lead-email').value = data.data.email;
      document.getElementById('add-edit-lead-phone').value = data.data.phone;
      document.getElementById('add-edit-lead-agent').value = data.data.agent;
      document.getElementById('add-edit-lead-property').value = data.data.property;
      if (data.data.unitPreference.bedrooms) {
        document.getElementById('add-edit-lead-bedrooms').value = Number(data.data.unitPreference.bedrooms);
      }
      if (data.data.unitPreference.bathrooms) {
        document.getElementById('add-edit-lead-bathrooms').value = Number(data.data.unitPreference.bathrooms);
      }
      if (data.data.unitPreference.leaseLength) {
        document.getElementById('add-edit-lead-lease-length').value = Number(data.data.unitPreference.leaseLength);
      }
      if (data.data.unitPreference.rentRangeMin) {
        document.getElementById('add-edit-lead-rent-range-min').value = Number(data.data.unitPreference.rentRangeMin);
      }
      if (data.data.unitPreference.rentRangeMax) {
        document.getElementById('add-edit-lead-rent-range-max').value = Number(data.data.unitPreference.rentRangeMax);
      }
      if (data.data.additionalInformation.moveInDate) {
        document.getElementById('add-edit-lead-movein-date').value = data.data.additionalInformation.moveInDate;
      }
      document.getElementById('add-edit-lead-lease-type').value = data.data.additionalInformation.leaseType;
      document.getElementById('add-edit-lead-model').value = data.data.unitPreference.floorPlan;
      document.getElementById('add-edit-lead-gender').value = data.data.additionalInformation.gender;
      document.getElementById('add-edit-lead-referrer').value = data.data.additionalInformation.referrer;
      document.getElementById('add-edit-lead-contact-method').value = data.data.additionalInformation.contactMethod;
    }

    closeClearModal() {
      let addEditLeadCloseBtn = document.getElementById('addEditLeadCloseBtn');
      document.getElementById('add-edit-lead-name').value = '';
      document.getElementById('add-edit-lead-email').value = '';
      document.getElementById('add-edit-lead-phone').value = '';
      document.getElementById('add-edit-lead-agent').value = '';
      document.getElementById('add-edit-lead-property').value = '';
      document.getElementById('add-edit-lead-bedrooms').value = '';
      document.getElementById('add-edit-lead-bathrooms').value = '';
      document.getElementById('add-edit-lead-lease-length').value = '';
      document.getElementById('add-edit-lead-rent-range-min').value = '';
      document.getElementById('add-edit-lead-rent-range-max').value = '';
      document.getElementById('add-edit-lead-model').value = '';
      document.getElementById('add-edit-lead-gender').value = '';
      document.getElementById('add-edit-lead-referrer').value = '';
      document.getElementById('add-edit-lead-lease-type').value = '';
      document.getElementById('add-edit-lead-movein-date').value = '';
      document.getElementById('add-edit-lead-contact-method').value = '';
      document.getElementById('modal-title-add-edit-lead').style.color = 'black';
      document.getElementById('modal-title-add-edit-lead').innerHTML = '';
      addEditLeadCloseBtn.click();
    }
    
    /**
     * 
     * @param {HTMLElement} id 
     * @param {String} errorMessage 
     */
    showError(id, errorMessage) {
      id.style.color = 'red';
      id.innerHTML = errorMessage;
    }

    open() {
      this.isopen = true;
      this.leadsTable.clearFilter();
      return Promise.resolve();
    }

    close() {
      return Promise.resolve();
    }
}

export default Leads;