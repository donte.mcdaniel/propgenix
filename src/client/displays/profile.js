import Display from '../display';

class Profile extends Display {
    constructor(ws) {
        super('profile');
        this.ws = ws;
        this.displayname = 'profile';
        console.log('Initializing profile');
    }

    open() {
        this.isopen = true;
        console.log('profile IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default Profile;