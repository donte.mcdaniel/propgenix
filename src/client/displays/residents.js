import Display from '../display';

class Residents extends Display {
    constructor(ws) {
        super('residents');
        this.ws = ws;
        this.displayname = 'residents';
        console.log('Initializing Residents');
    }

    open() {
        this.isopen = true;
        console.log('Residents IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default Residents;