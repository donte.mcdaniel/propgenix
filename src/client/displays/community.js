import Display from '../display';

class Community extends Display {
    constructor(ws) {
        super('community');
        this.ws = ws;
        this.displayname = 'community';
    }

    open() {
        this.isopen = true;
        console.log('Community IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default Community;