import Display from '../display';

class Dashboard extends Display {
    constructor(ws) {
        super('dashboard');
        this.ws = ws;
        this.displayname = 'dashboard';
        console.log('Initializing Dashboard');
    }

    open() {
        this.isopen = true;
        console.log('Dashboard IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default Dashboard;