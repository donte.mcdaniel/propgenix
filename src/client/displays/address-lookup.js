import Display from '../display';

class AddressLookup extends Display {
    constructor(ws) {
        super('address-lookup');
        this.ws = ws;
        this.displayname = 'address-lookup';
    }

    open() {
        this.isopen = true;
        console.log('AddressLookUp IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default AddressLookup;