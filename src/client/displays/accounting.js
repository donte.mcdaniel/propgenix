import Display from '../display';

class Accounting extends Display {
    constructor(ws) {
        super('accounting');
        this.ws = ws;
        this.displayname = 'accounting';
    }

    open() {
        this.isopen = true;
        console.log('Accounting IS OPEN');
        return Promise.resolve();
    }

    close() {
        return Promise.resolve();
    }
}

export default Accounting;