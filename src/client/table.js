'use strict'

import moment from 'moment';

let eventEmitter;

class Table {
  /**
   * 
   * @param {String} elementId id of the element
   */
  constructor(elementId) {
    this.table = new Tabulator(`#${elementId}`, {
      data: [],
      layout:"fitColumns",
      tooltips:true, //show tool tips on cells
	    addRowPos:"top",
      height:"500px",
      responsiveLayout: "hide",
      columns: [],
      rowClick:function(e, row) {
          //e - the click event object
          //row - row component
          eventEmitter.emit('rowData', row);// ._row
      },
    });
    this.eventEmitter;
  }

  selectRow(cell, formatterParams) {
    var value = cell.getValue();
    if(value.indexOf("o") > 0){
        return "<span style='color:red; font-weight:bold;'>" + value + "</span>";
    }else{
        return value;
    }
  }

  /**
   * 
   * @param {eventEmitter} emitter the eventEmitter instance
   */
  setEventEmitter(emitter) {
    this.eventEmitter = emitter;
    eventEmitter = emitter;
  }

  /**
   * 
   * @param {String} height Example: 500px
   */
  setTableHeight(height) {
    this.table.options.height = height;
    console.log(this.table);
  }

  /**
   * 
   * @param {String} title Required: The title that will be displayed in the column header
   * @param {String} columnKey Required: The key for this column in the data array
   * @param {Boolean} visible Determines if the column is visible. Default is true
   * @param {Boolean} sortable Makes the column sortable
   * @param {String} sorter Determines how to sort data in this column (string, number, boolean, datetime, date)
   * @param {Function} formatter Function with full control of the cell
   * @param {Number} width with of the column
   */
  addColumn(title, columnKey, visible, sortable, sorter, formatter, width, alignment) {
    if (!title) {
      return new Error('Add title to column');
    }
    if (!columnKey) {
      return new Error('Add columnKey to column');
    }

    let obj = {
      title:title,
      field:columnKey,
      visible:visible,
      sortable:sortable
    };

    if (width && width !== '') {
      obj.width = width;
    }

    if (alignment && alignment !== '') {
      obj.align = alignment;
    }

    if (sortable && sortable !== '') {
      obj.sorter = function(cell, formatterParams){sorter(cell,formatterParams)};
    }

    if (obj.sorter && obj.sorter === 'date') {
      obj.sorterParams = {format:"MM/DD/YYYY"};
    }

    if(formatter && formatter !== '') {
      obj.formatter = formatter;
    }

    this.table.addColumn(obj);
  }

  addSelectColumn() {
    let obj = {};
    let _this = this;
    obj.title = '';
    obj.field = 'select';
    obj.visisble = 'true';
    obj.sortable = false;
    obj.width = 30;
    obj.align = 'center';
    obj.cellClick = function(e, cell) {
      e.stopPropagation();
      if (cell._cell.element.querySelector('input').checked) {
        cell._cell.row.selected = true;
      }
      else {
        cell._cell.row.selected = false;
      }
      _this.updateActionButtons();
    };
    obj.formatter = function(cell, formatterParams) {
      let string = `<input class="custom-checkbox" rowSelector="true" type="checkbox"></>`;
      return string;
    };
    this.table.addColumn(obj);
  }

  /**
   * 
   * @param {Array} data Array of objects
   */
  addUpdateData(data) {
    this.table.updateOrAddData(data);
  }

  /**
   * 
   * @param {Array} data Array of objects
   */
  updateData(row, data) {
    this.table.updateRow(row, data);
  }
  
  // Custom filter example
  customFilter(data){
    return data.car && data.rating < 3;
  }

  //Trigger setFilter function with correct parameters
  updateFilter(value) {
    let filter = this.filterByValue.toLowerCase() == "function" ? customFilter : this.filterByValue.toLowerCase();
    this.table.setFilter(filter, 'like', value);
  }

  /**
   * 
   * @param {Element} filterByFieldElement Dropdown list element
   * @param {Element} filterValueElement Input field element
   */
  initializeFilter(defaultColumn, filterByFieldElement, filterValueElement, clearFilterButton) {
    this.filterByFieldElement = document.getElementById(filterByFieldElement);
    this.filterValueElement = document.getElementById(filterValueElement);
    this.clearFilterButton = document.getElementById(clearFilterButton);

    // TODO: Dynamically create filter elements

    this.defaultColumn = defaultColumn;
    this.filterByValue = defaultColumn;
    let c = document.getElementById(filterByFieldElement).children;
    for (let item of c) {
      item.addEventListener('click', (e) => {
        console.log(e.srcElement.innerText);
        this.filterByValue = e.srcElement.innerText;
      });
    }
    this.filterValueElement.addEventListener('keyup', (e) => {
      this.updateFilter(e.srcElement.value);
    });
    this.clearFilterButton.addEventListener('click', () => {
      this.clearFilter();
    });
  }

  clearFilter() {
    this.filterByValue = this.defaultColumn;
    this.filterValueElement.value = '';
    this.table.clearFilter();
  }
  
  /**
   * 
   * @param {String} format pdf, json, or csv 
   */
  downloadTable(format, reportName) {
    if (format === 'csv') {
      this.table.download("csv", "data.csv");
    }
    if (format === 'json') {
      this.table.download("json", "data.json");
    }
    if (format === 'pdf') {
      this.table.download("pdf", "data.pdf", {
        orientation:"portrait", //set page orientation to portrait
        title: reportName, //add title to report
      });
    }
  }

  updateActionButtons() {
    let rows = this.table.rowManager.rows;
    let rowSelected = false;
    rows.forEach(row => {
      if (row.selected) {
        rowSelected = true;
      }
    });
    if (!rowSelected) {
      document.getElementById('emailLeadBtn').setAttribute('disabled', '');
      document.getElementById('archiveLeadBtn').setAttribute('disabled', '');
    }
    else {
      document.getElementById('emailLeadBtn').removeAttribute('disabled');
      document.getElementById('archiveLeadBtn').removeAttribute('disabled');
    }
  }
}

export default Table;
