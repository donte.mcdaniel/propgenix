'use strict';

// In production, add websocket connection as an environment variable
import displayTransitioner from './display-transitioner';
import io from 'socket.io-client';

// All displays
import Dashboard from './displays/dashboard';
import Leads from './displays/leads';
import Residents from './displays/residents';
import Maintenance from './displays/maintenance';
import Accounting from './displays/accounting';
import Community from './displays/community';
import AddressLookup from './displays/address-lookup';

let socket = io();

socket.on('connect', function() {
  socket.emit('addTokensToConnection', {token: sessionStorage.getItem('token'), refreshToken: sessionStorage.getItem('refreshToken')});
  initializeDisplays(socket);
});

/**
 * 
 * @param {socket} socket Websocket Connection
 */
function initializeDisplays(socket) {
    // Adding the displays so they can receive websocket events
    // Websockets enable us to view data in realtime for the entire app.
    displayTransitioner.init();
    displayTransitioner.add(new Dashboard(socket));
    displayTransitioner.add(new Leads(socket));
    displayTransitioner.add(new Residents(socket));
    displayTransitioner.add(new Maintenance(socket));
    displayTransitioner.add(new Accounting(socket));
    displayTransitioner.add(new Community(socket));
    displayTransitioner.add(new AddressLookup(socket));
    displayTransitioner.transition('dashboard', 0, 0);
}
