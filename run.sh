#!/bin/bash

export AUTH_SECRET=samplesecret123456789
export REFRESH_TOKEN_SECRET=samplerefreshsecret123456789
export TOKEN_LIFE=60m
export REFRESH_TOKEN_LIFE=10m
export DATABASE_URL=mongodb://localhost:27017/propgenix

yarn start
